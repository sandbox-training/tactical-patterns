package com.basesoft.training.ddd.vo;

import com.basesoft.training.ddd.TacticalPatternApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TacticalPatternApplication.class)
@WebAppConfiguration
@DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
public class WhenCreatingClientShould {

    private MockMvc mvc;

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() throws Exception {
        this.mvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void refuseBecauseIllegalName() throws Exception {
        //given
        final String registerClient = registerCommnad("12312", "TEST-LOGIN", "TEST-STREET", "TEST-CITY", "25-614");

        //when
        final ResultActions perform = mvc.perform(post("/client/register")
                .contentType(contentType)
                .content(registerClient)
        );
        //then
        perform.andExpect(status().isBadRequest());
    }

    @Test
    public void refuseBecauseIllegalLogin() throws Exception {
        //given
        final String registerClient = registerCommnad("TEST-NAME", "$#@$@#$@", "TEST-STREET", "TEST-CITY",
                "25-614");

        //when
        final ResultActions perform = mvc.perform(post("/client/register")
                .contentType(contentType)
                .content(registerClient)
        );
        //then
        perform.andExpect(status().isBadRequest());
    }

    @Test
    public void refuseBecauseIllegalPostal() throws Exception {
        //given
        final String registerClient = registerCommnad("TEST-NAME", "TEST-LOGIN", "TEST-STREET", "TEST-CITY",
                "11111");

        //when
        final ResultActions perform = mvc.perform(post("/client/register")
                .contentType(contentType)
                .content(registerClient)
        );

        //then
        perform.andExpect(status().isBadRequest());
    }

    @Test
    public void beVisibleOnClientListening() throws Exception {
        //given
        final String registerClient = registerCommnad("TEST-NAME", "TEST-LOGIN", "TEST-STREET", "TEST-CITY",
                "11-111");

        //when
        mvc.perform(post("/client/register")
                .contentType(contentType)
                .content(registerClient)
        )
        .andExpect(status().isOk());

        //then
        mvc.perform(get("/client"))
           .andExpect(jsonPath("$", hasSize(1)))
           .andExpect(jsonPath("$[0].name", is("TEST-NAME")))
           .andExpect(jsonPath("$[0].login", is("TEST-LOGIN")))
           .andExpect(jsonPath("$[0].street", is("TEST-STREET")))
           .andExpect(jsonPath("$[0].city", is("TEST-CITY")))
           .andExpect(jsonPath("$[0].postal", is("11-111")));
    }

    private String registerCommnad(String name, String login, String street, String city, String postal){
        return new StringBuilder("{")
                .append("\"name\":\"").append(name).append("\",")
                .append("\"login\":\"").append(login).append("\",")
                .append("\"street\":\"").append(street).append("\",")
                .append("\"city\":\"").append(city).append("\",")
                .append("\"postal\":\"").append(postal).append("\"}")
                .toString();
    }
}
