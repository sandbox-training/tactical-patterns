package com.basesoft.training.ddd.order.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DisposeOrderItem {
    private Long productId;

    @JsonCreator
    public DisposeOrderItem(
            @JsonProperty("productId") Long productId
    ) {
        this.productId = productId;
    }
}
