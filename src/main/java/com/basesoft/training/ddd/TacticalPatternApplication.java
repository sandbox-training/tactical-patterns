package com.basesoft.training.ddd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TacticalPatternApplication {

	public static void main(String[] args) {
		SpringApplication.run(TacticalPatternApplication.class, args);
	}
}
