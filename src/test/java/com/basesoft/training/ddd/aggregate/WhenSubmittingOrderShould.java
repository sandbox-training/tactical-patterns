package com.basesoft.training.ddd.aggregate;

import com.basesoft.training.ddd.TacticalPatternApplication;
import com.jayway.jsonpath.JsonPath;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;

import static com.jayway.jsonpath.JsonPath.read;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TacticalPatternApplication.class)
@WebAppConfiguration
@DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
public class WhenSubmittingOrderShould {

    private MockMvc mvc;

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() throws Exception {
        this.mvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void changeStatusAtListing() throws Exception {
        //given
        final Integer clientId = createClient();
        mvc.perform(post("/order")
                .contentType(contentType)
                .content(createOrderCommand(clientId))
        )
        .andExpect(status().isOk());

        final String contentAsString = mvc.perform(get("/order"))
                                          .andReturn()
                                          .getResponse()
                                          .getContentAsString();
        Long id = read(contentAsString, "$[0].orderId");
        mvc.perform(post("/order/{id}/pick/product", id)
                .contentType(contentType)
                .content(pickProductCommand("1", "2"))
        );
        //when
        mvc.perform(post("/order/{id}/submit", id)
                .contentType(contentType)
        );


        //then
        mvc.perform(get("/order"))
           .andExpect(jsonPath("$", hasSize(1)))
           .andExpect(jsonPath("$[0].status", is("SUBMIT")));
    }


    private String createOrderCommand(Integer clientId){
        return new StringBuilder("{")
                .append("\"clientId\":\"").append(clientId)
                .append("\"}")
                .toString();
    }

    private String pickProductCommand(String productId, String quantity){
        return new StringBuilder("{")
                .append("\"productId\":\"").append(productId).append("\",")
                .append("\"quantity\":\"").append(quantity).append("\"}")
                .toString();
    }

    private String registerCommnad(String name, String login, String street, String city, String postal){
        return new StringBuilder("{")
                .append("\"name\":\"").append(name).append("\",")
                .append("\"login\":\"").append(login).append("\",")
                .append("\"street\":\"").append(street).append("\",")
                .append("\"city\":\"").append(city).append("\",")
                .append("\"postal\":\"").append(postal).append("\"}")
                .toString();
    }

    public Integer createClient() throws Exception{
        final String registerClient = registerCommnad("TEST-NAME", "TEST-LOGIN", "TEST-STREET", "TEST-CITY",
                "11-111");

        mvc.perform(post("/client/register")
                .contentType(contentType)
                .content(registerClient)
        )
           .andExpect(status().isOk());

        final String contentAsString = mvc.perform(get("/client"))
                                          .andReturn()
                                          .getResponse()
                                          .getContentAsString();
        return JsonPath.read(contentAsString, "$[0].id");
    }

}
