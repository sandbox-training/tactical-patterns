package com.basesoft.training.ddd.order.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChangeQuantity {

    private Long productId;
    private Long quantity;

    @JsonCreator
    public ChangeQuantity(
            @JsonProperty("productId") Long productId,
            @JsonProperty("quantity") Long quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }

    public Long getProductId() {
        return productId;
    }

    public Long getQuantity() {
        return quantity;
    }
}
