package com.basesoft.training.ddd.crm.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RegisterClient {
    String name;
    String login;
    String street;
    String city;
    String postal;

    @JsonCreator
    public RegisterClient(
            @JsonProperty("name") String name,
            @JsonProperty("login") String login,
            @JsonProperty("street") String street,
            @JsonProperty("city")String city,
            @JsonProperty("postal")String postal) {
        this.name = name;
        this.login = login;
        this.street = street;
        this.city = city;
        this.postal = postal;
    }
}
