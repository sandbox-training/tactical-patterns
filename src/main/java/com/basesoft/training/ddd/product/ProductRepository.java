package com.basesoft.training.ddd.product;

import java.util.Optional;

public interface ProductRepository {
    void add(Product product);
    Optional<Product> findOne(Long productId);
}
