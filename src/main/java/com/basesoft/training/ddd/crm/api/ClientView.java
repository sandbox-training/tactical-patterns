package com.basesoft.training.ddd.crm.api;

public class ClientView {

    public Long id;
    public String name;
    public String login;
    public String street;
    public String city;
    public String postal;

    public ClientView(Long id, String name, String login, String street, String city, String postal) {
        this.id = id;
        this.name = name;
        this.login = login;
        this.street = street;
        this.city = city;
        this.postal = postal;
    }
}
