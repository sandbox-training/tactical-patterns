package com.basesoft.training.ddd.order.domain;

import com.basesoft.training.ddd.order.api.OrderView;

public interface OrderEmail {
    void sendConfirmationEmail(OrderView orderView);
}
