package com.basesoft.training.ddd.product.infrastructure;


import com.basesoft.training.ddd.product.Product;
import com.basesoft.training.ddd.product.ProductRepository;
import org.springframework.stereotype.Service;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class InMemoryProductRepository implements ProductRepository{

    private List<Product> all = new ArrayList<>();

    {
        all.add(new Product(1L, "TEST-PRODUCT-1", new BigDecimal(10)));
        all.add(new Product(2L, "TEST-PRODUCT-2", new BigDecimal(20)));
        all.add(new Product(3L, "ILLEGAL-PRODUCT-3", new BigDecimal(30)));
    }

    @Override
    public void add(Product product) {
        throw new NotImplementedException();
    }

    @Override
    public Optional<Product> findOne(Long productId) {
        return all.stream()
                .filter(product -> product.getProductId().equals(productId))
                .findFirst();
    }
}
