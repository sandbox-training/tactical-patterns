package com.basesoft.training.ddd.order.infrastructure;

import java.util.Set;

public class HtmlEmail {
    private String from;
    private Set<String> to;
    private String subject;
    private String body;

    public HtmlEmail(String from, Set<String> to, String subject, String body) {
        this.from = from;
        this.to = to;
        this.subject = subject;
        this.body = body;
    }

    public String getFrom() {
        return from;
    }

    public Set<String> getTo() {
        return to;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }
}
