package com.basesoft.training.ddd.crm.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(value = "/client")
public class ClientApi {

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public @ResponseBody void register(@Valid @RequestBody RegisterClient registerClient){
        throw new NotImplementedException();
    }

    @RequestMapping(value = "/{id}/address", method = RequestMethod.POST)
    public @ResponseBody void changeAddress(@PathVariable("id") Long id, @Valid @RequestBody ChangeAddress
            changeAddress){
        throw new NotImplementedException();
    }


    @RequestMapping(value = "", method = RequestMethod.GET)
    public @ResponseBody List<ClientView> all(){
        throw new NotImplementedException();
    }

}
