package com.basesoft.training.ddd.crm.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ChangeAddress {
    public String street;
    public String city;
    public String postal;

    @JsonCreator
    public ChangeAddress(
            @JsonProperty("street") String street,
            @JsonProperty("city") String city,
            @JsonProperty("postal") String postal) {
        this.street = street;
        this.city = city;
        this.postal = postal;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public String getPostal() {
        return postal;
    }
}
