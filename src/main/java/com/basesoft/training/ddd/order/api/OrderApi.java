package com.basesoft.training.ddd.order.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

@Controller
@RequestMapping(value = "/order")
public class OrderApi {

    @RequestMapping(value = "/{orderId}/pick/product", method = RequestMethod.POST)
    public @ResponseBody void pick(@PathVariable Long orderId, @RequestBody PickProduct pickProduct){
        throw new NotImplementedException();
    }

    @RequestMapping(value = "/{orderId}/submit", method = RequestMethod.POST)
    public @ResponseBody void submit(@PathVariable Long orderId){
        throw new NotImplementedException();
    }

    @RequestMapping(value = "/{orderId}/item/quantity", method = RequestMethod.POST)
    public @ResponseBody void changeQuantity(@PathVariable Long orderId, @RequestBody ChangeQuantity changeQuantity){
        throw new NotImplementedException();
    }

    @RequestMapping(value = "/{orderId}/item/dispose", method = RequestMethod.POST)
    public @ResponseBody void disposeProduct(@PathVariable Long orderId, @RequestBody DisposeOrderItem disposeOrderItem){
        throw new NotImplementedException();
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public @ResponseBody void create(@RequestBody CreateOrder createOrder){
        throw new NotImplementedException();
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public @ResponseBody List<OrderView> all(){
        throw new NotImplementedException();
    }
}
