package com.basesoft.training.ddd.order.api;

import java.math.BigDecimal;

public class LineItem {
    private Long productId;
    private Long quantity;
    private BigDecimal price;

    public LineItem(Long productId, Long quantity, BigDecimal price) {
        this.productId = productId;
        this.quantity = quantity;
        this.price = price;
    }

    public Long getProductId() {
        return productId;
    }

    public Long getQuantity() {
        return quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
