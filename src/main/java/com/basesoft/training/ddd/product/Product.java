package com.basesoft.training.ddd.product;

import java.math.BigDecimal;

public class Product {
    private String name;
    private Long productId;
    private BigDecimal price;

    public Product(Long productId, String name, BigDecimal price) {
        this.name = name;
        this.productId = productId;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public Long getProductId() {
        return productId;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
