package com.basesoft.training.ddd.entity;

import com.basesoft.training.ddd.TacticalPatternApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;

import static com.jayway.jsonpath.JsonPath.read;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TacticalPatternApplication.class)
@WebAppConfiguration
@DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
public class WhenChangingOrderItemQuantityShould {

    private MockMvc mvc;

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() throws Exception {
        this.mvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void increaseQuantityOnProduct() throws Exception {
        //given
        mvc.perform(post("/order")
                .contentType(contentType)
        )
        .andExpect(status().isOk());

        final String contentAsString = mvc.perform(get("/order"))
                                          .andReturn()
                                          .getResponse()
                                          .getContentAsString();
        Long id = read(contentAsString, "$[0].orderId");
        mvc.perform(post("/order/{id}/pick/product", id)
                .contentType(contentType)
                .content(pickProductCommand("1", "2"))
        );

        //when
        mvc.perform(post("/order/{id}/item/quantity", id)
                .contentType(contentType)
                .content(changeQuantity("1", "3"))
        );


        //then
        mvc.perform(get("/order"))
           .andExpect(jsonPath("$", hasSize(1)))
           .andExpect(jsonPath("$[0].items[0].quantity", is(3)));
    }

    private String pickProductCommand(String productId, String quantity){
        return new StringBuilder("{")
                .append("\"productId\":\"").append(productId).append("\",")
                .append("\"quantity\":\"").append(quantity).append("\"}")
                .toString();
    }

    private String changeQuantity(String productId, String quantity){
        return new StringBuilder("{")
                .append("\"productId\":\"").append(productId).append("\",")
                .append("\"quantity\":\"").append(quantity).append("\"}")
                .toString();
    }

}
