package com.basesoft.training.ddd.order.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateOrder {
    private Long clientId;

    @JsonCreator
    public CreateOrder(
            @JsonProperty("clientId") Long clientId
    ) {
        this.clientId = clientId;
    }

    public Long getClientId() {
        return clientId;
    }
}
