package com.basesoft.training.ddd.order.api;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class OrderView {
    private Long orderId;
    private BigDecimal totalPrice;
    private String status;
    private Set<LineItem> items = new HashSet<>();

    public OrderView(Long orderId, BigDecimal totalPrice, String status, Set<LineItem> items) {
        this.orderId = orderId;
        this.totalPrice = totalPrice;
        this.status = status;
        this.items = items;
    }

    public Long getOrderId() {
        return orderId;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public Set<LineItem> getItems() {
        return items;
    }

    public String getStatus() {
        return status;
    }
}
