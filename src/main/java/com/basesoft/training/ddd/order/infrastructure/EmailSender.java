package com.basesoft.training.ddd.order.infrastructure;

public interface EmailSender {
    void send(HtmlEmail email);
}
